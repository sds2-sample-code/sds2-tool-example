from sds2.utility.gadget_protocol import StandAloneMultiEditScreen
from ToolSampleEdit import Edit


class ToolSampleRunner(object):
    def __init__(self):
        self._x = 12.
        self._y = 6.
        self._factory_run = False

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value
        
    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value

    @property
    def FactoryRun(self):
        """Indicate whether or not the tool is being run via modeling or
        from the factory method

        Note:
        -----
        If the tool is facotry run, skip the UI
        
        Returns:
            bool: True if run from the factory, False otherwise
        """

        return self._factory_run

    def Run(self):
        """Where the work is done
        """
        if not self.FactoryRun:
            if self.Edit():
                self.do_some_stuffs()
        else:
            # we skip UI here, the object should already be initialized from the
            # factory
            self.do_some_stuffs()
    
    def Edit(self):
        return StandAloneMultiEditScreen(
            Edit(self),
            [],
            Edit.StandAloneEditWindowID,
            "Tool Sample GUI"
        ).Run()

    @classmethod
    def Factory(cls, x, y):
        """Instantiate, populate, and return an object

        For use from another custom object. 
        
        Returns:
            ToolSample: An instantiated object with defaults filled
        """
        f = cls()
        # set relevant attributes here
        f.FactoryRun = True
        f.x = x
        f.y = y
        return f

    def do_some_stuffs(self):
        """This is where you would add some objects
        """
        pass