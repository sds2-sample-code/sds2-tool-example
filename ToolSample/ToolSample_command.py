import os

from Commands import AvailableTools
from Commands import CmdArgs
from Commands import Command
from Commands import CommandType
from Commands import Icon
from Commands import IconSet
from Commands import Listing
from Commands import OperationClass
from Commands import Station

class ToolSample(Command):
    exec_file = os.path.join(
        os.path.dirname(__file__),
        "ToolSample_runner.py"
    )

    UserStr = "User Visible Name"

    def __init__(self):
        """This init method is used to initialize the base class
        """        
        Command.__init__(  # This initializes the base class Command
            # This passes the current instance (self) to the base class init
            self,
            # this indicates which type of operation the command performs
            # in this case we choose Param, which is for parametrics (the python API)
            operations=OperationClass.Param,
            # indicates where the tool can be listed
            # choices are ['All', 'Empty', 'Key', 'Menu', 'Mode', 'ShortcutMenu', 'Toolbar']
            listings=Listing.All,
            # a tuple of Icon objects that indicate where icon images are located
            # example: Icon(os.path.join('directory-name', 'image-name'))
            icons=IconSet(tuple()),
            # long form description of the tool
            long_description="User Visible Name",
            # command name for UI
            command_text="""User Visible Name""",
            # hover text for UI
            alt_text="""User Visible Name""",
            # any documentation; usually left blank internally
            documentation="""""",
            # indicates whether the command can run in a clone process
            # this is always false for parametric tools
            clone=False,  
            # which stations can run the tool
            stations=Station.Frame | Station.FrameReview | Station.FrameBim | Station.FrameErector | Station.FrameErectorPlus | Station.FrameApproval | Station.FrameFabricator | Station.FrameModelstn | Station.FrameDraftstn | Station.FrameLite | Station.FrameErectorPlus,
            # Which type of command; parametric tools will always use CommandType.Command
            type=CommandType.Command
        )

    def Invoke(self, args):
        """Run the associated python code for the tool

        Args:
            args (list): list of arguments passed to Invoke; not normally used for parametric tools
        """        
        args = CmdArgs(parametric_file=self.exec_file)
        AvailableTools.param_run(args)
