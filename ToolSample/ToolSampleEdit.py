from sds2.utility.gadget_protocol import MultiEditable
from sds2.utility.gadget_protocol import StandAloneMultiEditScreen
from sds2.utility.gadget_protocol import SubdialogController
from dialog.entry import Entry
from dialog.dimension import DimensionStyled


screen_id = '98fde6ef-9941-49d5-ba83-bc7daef21389'

class Edit(MultiEditable):
    StandAloneEditWindowID = '9f04d573-2e4d-4111-a060-37d41b66ed2c'

    def __init__(self, model):
        self.model = model

    def CreateCustomMultiEditableUI(self, __, factory):
        build_ui([self.model,], factory)
        

def settings_leaf(parent):
    Entry(parent, '_x', DimensionStyled(), 'x:')
    Entry(parent, '_y', DimensionStyled(), 'y:')

def build_ui(model, gadget_factory):
    controller = SubdialogController(model)
    column = gadget_factory.Column(
        controller,
        None,
        "Tool Sample Column",
        '',
        'a523056f-ee56-4974-bd57-4bb320e4d2da',  # this is a unique id for the leaf
                                                 # so we can remember it's open/closed state
    )

    gadget_factory.Leaf(
        column,
        controller,
        settings_leaf,
        'General Settings',
        '',
        '48fb7fa2-abe0-45c8-82d1-d0dabe7d1b4c'
    )